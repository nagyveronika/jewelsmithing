import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{
	//private Repository repo = new Repository();
	
	public static void main(String[] args) {
		launch(args);
		
		}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent load = FXMLLoader.load(getClass().getResource("\\gui\\view.fxml"));
		Scene value = new Scene(load);
		primaryStage.setScene(value);
		primaryStage.setTitle("�tv�ss�g");
		primaryStage.show();
		
	}
}