package alloying;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Gold;
import entities.Silver;

public class FinenessCalculationFormulas {
	private final char percent = '%';
	private Map<Integer, Double>finenessOfKarat = new HashMap<Integer, Double>();
	private Gold gold = new Gold();
	private Silver silver = new Silver();
	public Map<Integer, Double> finenessConversion() {
		//a map that contains all of the 24 karat elements to fineness%
		//rounding double to two decimal number: decimal format, round()
		gold.setKarat(16);
		int karat = gold.getKarat();
		double twofour = 24.00;
		double finenessOfTheGoldItem = karat*1000/twofour;
		for (int i = 1; i <= 24; i++) { //how to round to 2 decimals
			finenessOfKarat.put(i, i*1000/twofour);	
		}
		return finenessOfKarat;
	/*-------------Silver-----------------*/
		
	}
	
	private Double round(double d, int i) {
		// TODO Auto-generated method stub
		return null;
	}

	public double pureGoldWeightFromAnAlloy() {
	//how much pure gold(s) is in a ...gramm, karat gold item
		//rounding double to one decimal number
		gold.setWeight(5.5);
		gold.setKarat(18);
		double weight = gold.getWeight();
		int karat = gold.getKarat();
		double pureWeight = 0;
		if (finenessOfKarat.containsKey(karat)) {
			pureWeight = weight*finenessOfKarat.get(karat).doubleValue()/1000;
		}
		
		return pureWeight;
	}

	
	public char getPercent() {
		return percent;
	}
	
}
