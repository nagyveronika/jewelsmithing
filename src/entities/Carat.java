package entities;

public class Carat {
	int carat;
	double perthousand;

	public Carat() {

	}

	public Carat(int carat, double perthousand) {
		this.carat = carat;
		this.perthousand = perthousand;
	}

	public int getCarat() {
		return carat;
	}

	public void setCarat(int carat) {
		this.carat = carat;
	}

	public double getPerthousand() {
		return perthousand;
	}

	public void setPerthousand(double perthousand) {
		this.perthousand = perthousand;
	}

	@Override
	public String toString() {
		return "Carat [carat=" + carat + ", perthousand=" + perthousand + "]";
	}
	


	
}
