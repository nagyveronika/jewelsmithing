package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class MetalObjects{
	
	private Scanner scan = new Scanner(System.in);
	private List<Gold> goldList = new ArrayList<>();
	private List<Silver> silverList = new ArrayList<>();
	private String nameOfMetal;
	
	//Constructor for newMetalObject(One object is ideal from this class)
	public MetalObjects(String nameOfMetal) {
		this.nameOfMetal = nameOfMetal;
	}
	//checking goldList.size(), adding a sample element into it.
	private void goldListSize() {
		int goldListSize = goldList.size();
		System.out.println("Your List of Gold has " + goldListSize + " elements in it.");
		if (goldListSize == 0) {
			Gold sampleGold = new Gold();
			goldList.add(sampleGold);
		}
	}
	
	public List<Gold> addNewGoldItem(String nameOfMetal) {
		//checks *the metalOfName*
		// check doesn't needed anymore
		// also making addNewSilver item method 
		// so You can choose from the menu which ever new Metal Item you want to add
		nameOfMetal = scan.next();
		if (nameOfMetal != null) {
			System.out.println("Give me the karat of your Gold item...");
			int karat = scan.nextInt();
			System.out.println("Give me the weight of your Gold item...");
			double weight = scan.nextDouble();
			Gold gold = new Gold(nameOfMetal, karat, weight);
			goldList.add(gold);
		}

		return goldList;
		
	}
	public List<Silver> addNewSilverItem(String nameOfMetal){
		nameOfMetal = scan.next();
		if (nameOfMetal != null) {
			System.out.println("Give me the lat of your Silver item...");
			int lat = scan.nextInt();
			System.out.println("Give me the weight of your Silver item...");
			double weight = scan.nextDouble();
			Silver silver = new Silver(nameOfMetal, lat, weight);
			silverList.add(silver);
		}
		return silverList;
	}
	//Getters & Setters
	public String getNameOfMetal() {
		return nameOfMetal;
	}
	public void setNameOfMetal(String nameOfMetal) {
		this.nameOfMetal = nameOfMetal;
	}
	public List<Gold> getGoldList() {
		return goldList;
	}
	public void setGoldList(List<Gold> goldList) {
		this.goldList = goldList;
	}
	
}
