package entities;

public class Gold {
	private String name;
	private int karat;
	private double weight;
	private final int pureGold = 24;
	private double finenessOfGold = 0;
	public Gold() {}
	
	public Gold(String name, int karat, double weight) {
		this.name = name;
		this.karat = karat;
		this.weight = weight;
	}
	public int pureGold() {
//		if () {
//			
//		}
		return 0;
	}

	public int getKarat() {
		return karat;
	}

	public void setKarat(int karat) {
		this.karat = karat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	
	public void setFinenessOfGold(double finenessOfGold) {
		this.finenessOfGold = finenessOfGold;
	}

	public double getFinenessOfGold() {
		return finenessOfGold;
	}

	@Override
	public String toString() {
		return "GoldItem [name= " + name + ", karat= " + karat + ", weight= " + weight + "g ]";
	}

	
	
	
}
