package entities;

public class Silver {
 //pure silver = 16 lat = 1000%
 //16 lat = 1000;
 //1 lat = 1000/16%
 //13 lat = 1000*13/16 = 812,5%
	private String name;
	private int lat;
	private double weight;
	
	public Silver() {}
	public Silver(String name, int lat, double weight) {
		this.name = name;
		this.weight = weight;
		this.lat = lat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getLat() {
		return lat;
	}

	public void setLat(int lat) {
		this.lat = lat;
	}

	@Override
	public String toString() {
		return "Silver [name=" + name + ", weight=" + weight + ", lat=" + lat + "]";
	}
	
	
	
	
}
