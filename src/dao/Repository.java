package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Carat;

public class Repository {
	private final String URL = "jdbc:mysql://localhost:3306/jewelsmithing";
	private final String USER_NAME = "root";
	private final String PASSWORD = "";
	private Connection con = null;
	private Statement stmt = null;
	private List<Carat> caratList = new ArrayList<Carat>();
	
	public Repository() {
		startDataBase();
	}
	
	private void startDataBase() {
		try {
			con = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
			stmt = con.createStatement();
			System.out.println("Connecting to Database...");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void closeDataBase() {
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	public List<Carat> findAllCarat() {
		String query = "SELECT * FROM karatperthousands";
		try {
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				Carat carat = new Carat(rs.getInt("karat"), rs.getDouble("perthousand"));
				caratList.add(carat);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return caratList;
	}
	
}
