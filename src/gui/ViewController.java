package gui;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import dao.Repository;
import entities.Carat;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class ViewController implements Initializable{
	private Repository repo;
	@FXML
	private AnchorPane rootPane;
	@FXML
	private Pane pane;
	@FXML
	private Label label;
	@FXML
	private MenuItem alloyCalc1, karatCalc;
	@FXML
	private TableView<Carat> caratTable;
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		repo = new Repository();
		initCaratTable();
		fillCaratTable();
	}

	@FXML
	private void showAlloyCalc1() {
		pane.setVisible(true);
	}
	
	@FXML
	private void showKaratTable() {
		caratTable.setVisible(true);
		
	}

	private void initCaratTable() {
		TableColumn<Carat, Integer> karatCol = new TableColumn<>("Kar�t");
		TableColumn<Carat, Double> perThousandsCol = new TableColumn<>("Ezrel�k(%)");
		
		karatCol.setCellValueFactory(new PropertyValueFactory<>("carat"));
		perThousandsCol.setCellValueFactory(new PropertyValueFactory<>("perthousand"));
		caratTable.getColumns().addAll(karatCol, perThousandsCol);
	}
	
	private void fillCaratTable() {
		caratTable.setItems(FXCollections.observableArrayList(repo.findAllCarat()));
	}
	
}
