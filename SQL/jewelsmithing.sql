-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Már 31. 14:27
-- Kiszolgáló verziója: 10.4.11-MariaDB
-- PHP verzió: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `jewelsmithing`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `karatperthousands`
--

CREATE TABLE `karatperthousands` (
  `karat` int(11) NOT NULL,
  `perthousand` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `karatperthousands`
--

INSERT INTO `karatperthousands` (`karat`, `perthousand`) VALUES
(0, '0.02'),
(1, '41.69'),
(2, '83.35'),
(3, '125.02'),
(4, '166.68'),
(5, '208.35'),
(6, '250.01'),
(7, '291.68'),
(8, '333.34'),
(9, '375.01'),
(10, '416.67'),
(11, '458.34'),
(12, '500.00'),
(13, '541.67'),
(14, '583.33'),
(15, '625.01'),
(16, '666.68'),
(17, '708.34'),
(18, '750.01'),
(19, '791.67'),
(20, '833.34'),
(21, '875.00'),
(22, '916.67'),
(23, '958.33'),
(24, '1000.00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
